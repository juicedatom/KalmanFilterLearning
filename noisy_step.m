%Simple kalman filter for 1-d voltage fun

close
clear all

%generate our control signal (this is what we tell the robot to do)
n = 10000;
%at time t = 2500, go up by 15
U = zeros(1,n);

U(1500) = -250;
U(2500) = 100;
U(3000) = -300;
U(8000) = 500;


%add some noise
sigma = 300;

Actual = zeros(1,n);
Actual(1) = 400;
for ii=2:n
	Actual(ii) = U(ii) + Actual(ii-1);
end

Z = Actual + normrnd(0,sigma,[1 n]);

A = 1;
B = 1;

%make this really small, for this example we'll trust the control
Q = 0.000001;
%Q = 0;

%this is going to be based on our generated data;
R = 0.1;
H = 1;

X = jkalman(Z, U, A, B, R, Q, H);

figure
hold on

scatter(1:length(Z),Z, 'b');
plot(X, 'r', 'linewidth', 2);
plot(Actual, 'k--')

title('kalman filter example');
legend('Sampled Points', 'Estimated Position', 'Actual');

xlabel('time');
ylabel('position')
