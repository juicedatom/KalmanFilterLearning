%josh's kalman filter program

function [X] = jkalman(Z, U, A, B, R, Q, H)

n = length(Z);

X = zeros(1,n);
P = zeros(1,n);

for ii=2:n
	Xp = A*X(ii-1) + B*U(ii);
	Pp = A*P(ii-1)*A' + Q;

	y = Z(ii) - H*Xp;
	S = H*Pp*H' + R;
	K = Pp*H'/S;
	X(ii) = Xp + K*y;

	temp = K*H;
	I_sub = eye*length(temp);
	P(ii) = (I_sub - K*H)*Pp;
end

end
